/* eslint-env browser */

async function getNamesByPage(page, selector = 'td.RegPortErg_FirmaKopf') {
	return await page.evaluate(selector => {
		let tableData = Array.from(document.querySelectorAll(selector));
		// the table names
		return tableData.map(data => data.textContent);
	}, selector);
}

export default getNamesByPage;
