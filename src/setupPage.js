import puppeteer from 'puppeteer';

const CONFIG = {
	launch: { headless: false },
	viewport: { width: 1024, height: 768 }
};

async function setupPage() {
	const browser = await puppeteer.launch(CONFIG.launch);
	const page = await browser.newPage();
	await page.setViewport(CONFIG.viewport);
	await page.goto('https://www.handelsregister.de/rp_web/mask.do?Typ=e');

	return { page, browser };
}

export default setupPage;
