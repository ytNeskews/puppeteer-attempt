async function setFiltersAndNavigate(page, resultsPerPage = 100) {
	await Promise.all([
		page.click('input#landNW'), // set a filter
		page.select('select[name="ergebnisseProSeite"]', resultsPerPage.toString()), // set another filter
		page.click('input[type=submit]'), // submit the form
		page.waitForNavigation()
	]).catch(e => console.log(e));
}

export default setFiltersAndNavigate;
