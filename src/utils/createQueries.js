const RESULT_BASE = 'https://www.handelsregister.de/rp_web/result.do';

function createQueries(resultsPerPage, by) {
	console.log('resultsPerPage are set to', resultsPerPage);
	console.log('it is muliplied by', by);
	const LIMIT = (200 * by ** 2) / resultsPerPage;
	console.log(
		'settings lead to a limit of ',
		`200 x ${by} ** 2 / ${resultsPerPage} = ${LIMIT}`
	);

	let queries = [];
	for (let i = 1; i <= LIMIT; i++) {
		queries = [...queries, `${RESULT_BASE}?Page=${i}`];
	}
	console.log('created', queries.length, 'queries');
	return queries;
}

export default createQueries;
