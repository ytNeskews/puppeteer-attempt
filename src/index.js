import fs from 'fs';
import setupPage from './setupPage';
import setFiltersAndNavigate from './setFiltersAndNavigate';
import multiply from './multiply';
import mergeTotalNames from './mergeTotalNames';

const RESULTS_PER_PAGE = 100;
const BY = 2;

(async () => {
	const { page, browser } = await setupPage(); // get the page object

	// set the needed filters on the page and then navigate by submitting the form
	await setFiltersAndNavigate(page, RESULTS_PER_PAGE);

	// click BY times on "double the results" link to load 200 * BY ** 2 / resultsPerPage names
	await multiply(page, BY);

	// get the names on every page
	const names = await mergeTotalNames(page, RESULTS_PER_PAGE, BY);

	// write companies to file
	fs.writeFileSync('companies.txt', names.join('\r\n'));

	await browser.close();
})();
