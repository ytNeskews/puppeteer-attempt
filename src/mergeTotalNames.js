import getNamesByPage from './getNamesByPage';
import createQueries from './utils/createQueries';

async function mergeTotalNames(page, resultsPerPage, by) {
	const queries = createQueries(resultsPerPage, by);

	let names = [];

	for (let i = 0; i < queries.length; i++) {
		names = [...names, ...(await getNamesByPage(page))];
		await page.goto(queries[i], 'networkidle0');
	}

	console.log('found', names.length, 'companies');
	return names;
}

export default mergeTotalNames;
