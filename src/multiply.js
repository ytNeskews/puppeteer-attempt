async function multiply(page, by) {
	console.log('expect to click on multiply', by, 'times');
	for (let i = 0; i < by; i++) {
		await Promise.all([
			// Manual clicking of the link
			page.$eval('p a', el => el.click()),
			page.waitForNavigation()
		]).catch(e => console.log(e));
	}
}

export default multiply;
